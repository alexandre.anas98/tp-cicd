/*Commentaire de AlexG*/
package com.spring.todo.todoapp.controllers;

import com.spring.todo.todoapp.models.dto.TaskRequest;
import com.spring.todo.todoapp.models.dto.TaskResponse;
import com.spring.todo.todoapp.models.entity.Task;
import com.spring.todo.todoapp.services.TaskService;
import com.spring.todo.todoapp.services.mappers.TaskMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/api/tasks")
public class TaskController {

    final TaskService taskService;
    final TaskMapper taskMapper;

    @Autowired
    public TaskController(final TaskService taskService, final TaskMapper taskMapper) {
        this.taskService = taskService;
        this.taskMapper = taskMapper;
    }

    @ApiOperation(value = "Get the full list of task", response = TaskResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = TaskResponse.class, responseContainer = "List")
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskResponse> getAll() {
        return taskMapper.toSimpleResponseList(taskService.getAll());
    }


    @ApiOperation(value = "Create a new Task", response = TaskResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Ok", response = TaskResponse.class),
        @ApiResponse(code = 400, message = "Bad request")
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskResponse> createTask(@RequestBody TaskRequest taskRequest) {
        if(taskRequest == null || StringUtils.isBlank(taskRequest.getDescription())) {
            return ResponseEntity.badRequest().build();
        }

        Task task = taskService.create(taskRequest);

        URI resourceLocation = ServletUriComponentsBuilder
            .fromCurrentContextPath()
            .path("/api/tasks/" + task.getId())
            .build()
            .toUri();

        return ResponseEntity.created(resourceLocation).body(taskMapper.toSimpleResponse(task));
    }


    @ApiOperation(value = "Get task by id", response = TaskResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = TaskResponse.class),
        @ApiResponse(code = 404, message = "Not Found")
    })
    @GetMapping(value = "{taskId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskResponse> getById(@PathVariable("taskId") Long taskId) {
        if(!taskService.exists(taskId)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(taskMapper.toSimpleResponse(taskService.getById(taskId)));
    }

    @ApiOperation(value = "µUpdate task by id", response = TaskResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = TaskResponse.class),
        @ApiResponse(code = 404, message = "Not Found")
    })
    @PatchMapping(value = "{taskId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateTask(@PathVariable("taskId") Long taskId, @RequestBody TaskRequest taskRequest) {
        if(!taskService.exists(taskId)) {
            return ResponseEntity.notFound().build();
        }

        this.taskService.update(taskId, taskRequest);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Delete task by id", response = TaskResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = TaskResponse.class),
        @ApiResponse(code = 404, message = "Not Found")
    })
    @DeleteMapping(value = "{taskId}")
    public ResponseEntity<Void> updateTask(@PathVariable("taskId") Long taskId) {
        if(!taskService.exists(taskId)) {
            return ResponseEntity.notFound().build();
        }

        this.taskService.delete(taskId);
        return ResponseEntity.ok().build();
    }
}
